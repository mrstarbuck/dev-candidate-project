/**
 * @description Contains business logic for the OpportunityTrigger
 * @author Jason Burke
 * @date 6/29/2021
 */
public with sharing class OpportunityTriggerHandler {
    public static Boolean isFirstRun = true;

    /**
     * @description Creates renewal Opportunities with Products for newly Closed/Won Opportunities
     * @param newOppList The updated Opportunities
     * @param oldOppMap The map of Opportunity Id to the pre-update version of its Opportunity, or null if after insert
     */
    public static void createRenewalOpportunity(Opportunity[] newOppList, Map<Id, Opportunity> oldOppMap) {
        /*
         * Instead of looking for the Opportunities with a StageName of "Closed Won", we are finding all of the OpportunityStages
         * that have IsClosed and IsWon equal to true.  Then we look for the Opportunities that are in one of those Stages.
         * This way, if the Closed/Won Stages are modified later, this code will still work.
         */
        // set of the names of all the Opportunity Stages that have Closed/Won type
        Set<String> closedWonOppStageNameSet = new Set<String>();
        // name of the first Opportunity Stage
        String firstOppStageName = null;
        // loop through all active Opportunity Stages in order and populate above variables
        for (OpportunityStage oppStage : [
                SELECT ApiName, IsClosed, IsWon
                FROM OpportunityStage
                WHERE IsActive = TRUE
                ORDER BY SortOrder ASC
        ]) {
            if (firstOppStageName == null) {    // only on first loop
                firstOppStageName = oppStage.ApiName;
            }
            if (oppStage.IsClosed && oppStage.IsWon) {
                closedWonOppStageNameSet.add(oppStage.ApiName);
            }
        }

        // Now loop through the updated Opportunities and find those that are newly in a Closed/Won stage.
        List<Opportunity> closedWonOppList = new List<Opportunity>();
        for (Opportunity newOpp : newOppList) {
            if (closedWonOppStageNameSet.contains(newOpp.StageName)) {
                Opportunity oldOpp = oldOppMap?.get(newOpp.Id);
                if (oldOpp == null || (newOpp.StageName != oldOpp.StageName && !closedWonOppStageNameSet.contains(oldOpp.StageName))) {
                    closedWonOppList.add(newOpp);
                }
            }
        }

        if (closedWonOppList.isEmpty()) {
            // nothing to do
            return;
        }

        // create the Renewal Opportunities and tie them to the original Opportunities that they are renewing
        Map<Id, Opportunity> closedWonToRenewalMap = new Map<Id, Opportunity>();
        for (Opportunity closedWonOpp : closedWonOppList) {
            Opportunity renewalOpp = closedWonOpp.clone();
            renewalOpp.CloseDate = closedWonOpp.CloseDate.addYears(1);
            renewalOpp.StageName = firstOppStageName;
            renewalOpp.Type = Constants.OPP_TYPE_RENEWAL;
            closedWonToRenewalMap.put(closedWonOpp.Id, renewalOpp);
        }

        // after this DML statement the Renewal Opportunities in the map will have their Ids set
        // not catching exceptions because we want everything rolled back if there's an error
        insert closedWonToRenewalMap.values();

        // Create Opportunity Products for the Renewal Opportunities
        List<OpportunityLineItem> renewalOppProductList = new List<OpportunityLineItem>();
        for (OpportunityLineItem oppProduct : [
                SELECT OpportunityId, Product2Id, Quantity, UnitPrice
                FROM OpportunityLineItem
                WHERE OpportunityId IN :closedWonOppList
        ]) {
            // get the matching Renewal Opportunity for this Opportunity Product
            Opportunity renewalOpp = closedWonToRenewalMap.get(oppProduct.OpportunityId);
            // add the new Opportunity Product for the Renewal Opportunity
            renewalOppProductList.add(new OpportunityLineItem(
                    OpportunityId = renewalOpp.Id,
                    Product2Id = oppProduct.Product2Id,
                    Quantity = oppProduct.Quantity,
                    UnitPrice = oppProduct.UnitPrice,
                    Discount = 0
            ));
        }

        // not catching exceptions because we want everything rolled back if there's an error
        insert renewalOppProductList;
    }
}