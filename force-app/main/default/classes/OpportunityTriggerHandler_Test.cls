@IsTest
private class OpportunityTriggerHandler_Test {

    @TestSetup
    static void setup() {
        Opportunity opp = new Opportunity(
                Name = 'Test Opportunity',
                CloseDate = Date.today(),
                StageName = getFirstStageName()
        );
        insert opp;

        Product2 product = new Product2(
                Name = 'Test Product',
                ProductCode = 'ABC123',
                IsActive = true
        );
        insert product;

        PricebookEntry pbEntry = new PricebookEntry(
                Pricebook2Id = Test.getStandardPricebookId(),
                Product2Id = product.Id,
                UnitPrice = 100,
                IsActive = true
        );
        insert pbEntry;

        Integer quantity = 3;
        OpportunityLineItem oppProduct = new OpportunityLineItem(
                OpportunityId = opp.Id,
                PricebookEntryId = pbEntry.Id,
                Quantity = quantity,
                TotalPrice = quantity * pbEntry.UnitPrice
        );
        insert oppProduct;
    }

    @IsTest
    static void testCreateRenewalOpportunity() {
        OpportunityStage closedWonStage = [
                SELECT ApiName FROM OpportunityStage
                WHERE IsActive = TRUE
                AND IsClosed = TRUE
                AND IsWon = TRUE
                ORDER BY SortOrder ASC
                LIMIT 1
        ];

        Opportunity opp = [SELECT Id FROM Opportunity];
        opp.StageName = closedWonStage.ApiName;

        Test.startTest();
        {
            update opp;
        }
        Test.stopTest();

        Opportunity[] oppList = [
                SELECT CloseDate, StageName, Type,
                    (SELECT Product2Id, Quantity, UnitPrice, Discount FROM OpportunityLineItems)
                FROM Opportunity
        ];
        System.assertEquals(2, oppList.size());
        opp = oppList[0].Id == opp.Id ? oppList[0] : oppList[1];
        Opportunity renewalOpp = oppList[0].Id != opp.Id ? oppList[0] : oppList[1];
        System.debug(opp);
        System.debug(opp.OpportunityLineItems);
        System.debug(renewalOpp);
        System.debug(renewalOpp.OpportunityLineItems);

        System.assertEquals(opp.CloseDate.addYears(1), renewalOpp.CloseDate);
        System.assertEquals(getFirstStageName(), renewalOpp.StageName);
        System.assertEquals(Constants.OPP_TYPE_RENEWAL, renewalOpp.Type);

        System.assertEquals(1, opp.OpportunityLineItems.size());
        System.assertEquals(1, renewalOpp.OpportunityLineItems.size());
        System.assertEquals(opp.OpportunityLineItems[0].Product2Id, renewalOpp.OpportunityLineItems[0].Product2Id);
        System.assertEquals(opp.OpportunityLineItems[0].Quantity, renewalOpp.OpportunityLineItems[0].Quantity);
        System.assertEquals(opp.OpportunityLineItems[0].UnitPrice, renewalOpp.OpportunityLineItems[0].UnitPrice);
        System.assertEquals(0, renewalOpp.OpportunityLineItems[0].Discount);
    }

    private static String getFirstStageName() {
        OpportunityStage firstStage = [
                SELECT ApiName FROM OpportunityStage
                WHERE IsActive = TRUE
                ORDER BY SortOrder ASC
                LIMIT 1
        ];

        return firstStage.ApiName;
    }
}