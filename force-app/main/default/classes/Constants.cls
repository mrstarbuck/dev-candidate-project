/**
 * Created by Jason Burke on 6/29/2021.
 */

public with sharing class Constants {

    public static final String OPP_TYPE_RENEWAL = 'Renewal';
}