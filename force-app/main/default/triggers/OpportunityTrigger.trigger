/**
 * Created by Jason Burke on 6/29/2021.
 */

trigger OpportunityTrigger on Opportunity (after insert, after update) {

    if (OpportunityTriggerHandler.isFirstRun) {
        OpportunityTriggerHandler.isFirstRun = false;
        OpportunityTriggerHandler.createRenewalOpportunity(Trigger.new, Trigger.oldMap);
    }

}